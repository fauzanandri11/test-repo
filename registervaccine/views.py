from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .forms import FormPendaftaran
from django.http.response import HttpResponse
from django.core import serializers
from .models import DataPendaftar
from kalender.models import Event
from django.http import JsonResponse
import datetime

# Create your views here.
@login_required(login_url='login')
def index(request):
    context = {}
    form = FormPendaftaran(request.POST or None)
    form.fields['username'].initial = request.user

    tanggal = request.GET.get('tanggal')
    bulan = request.GET.get('bulan')
    tahun = request.GET.get('tahun')

    if (type(tanggal) is str):
        intended_tanggal = tanggal + ' ' + bulan + ' ' + tahun
        form.fields['tanggal_vaksin'].initial = datetime.datetime.strptime(intended_tanggal, '%d %m %Y')

    if (form.is_valid and request.method == 'POST'):
        form.save()

    context['form'] = form
    return render(request, 'register_vaccine.html', context)

def daftar(request, event_id):
    context = {}
    form = FormPendaftaran(request.POST or None)
    event = Event.objects.get(id=event_id)
    waktu = event.start_time.strftime("%H:%M") + " - " + event.end_time.strftime("%H:%M")

    form.fields['username'].initial = request.user
    form.fields['event'].initial = event_id
    form.fields['tanggal_vaksin'].initial = event.date
    form.fields['waktu_vaksin'].initial = waktu

    tanggal = request.GET.get('tanggal')
    bulan = request.GET.get('bulan')
    tahun = request.GET.get('tahun')

    if (type(tanggal) is str):
        intended_tanggal = tanggal + ' ' + bulan + ' ' + tahun
        form.fields['tanggal_vaksin'].initial = datetime.datetime.strptime(intended_tanggal, '%d %m %Y')

    if (form.is_valid and request.method == 'POST'):
        form.save()

    context = {
        'form': form,
        'event': event
    }

    return render(request, 'register_vaccine.html', context)

@login_required(login_url='login')
def json(request):
    # Pake ini di AJAX, trus diubah jadi object javascript, tinggal dipake
    # buat nampilin yang udah didaftarin di html-nya
    data = serializers.serialize('json', DataPendaftar.objects.filter(username=request.user))
    return HttpResponse(data, content_type="application/json")

def delete(request):
    data = list(DataPendaftar.objects.values())
    pk_number = request.GET.get('pk')
    DataPendaftar.objects.filter(id=int(pk_number)).delete()
    return JsonResponse(data, safe=False)