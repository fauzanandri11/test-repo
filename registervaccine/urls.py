from django.urls import path
from .views import index, json, delete, daftar

app_name = 'registervac'
urlpatterns = [
    path('', index, name='index'),
    path('daftar/<event_id>', daftar, name='daftar'),
    path('json', json, name='json'),
    path('json', json, name='json_vaccine'),
    path('delete/', delete, name='delete')
]