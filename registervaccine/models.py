from django.db import models
from kalender.models import Event

# Create your models here.
class DataPendaftar(models.Model):
    username = models.CharField(max_length=100)
    event = models.CharField(max_length=100)
    tanggal_vaksin = models.DateField()
    waktu_vaksin = models.CharField(max_length=100)
    NIK = models.BigIntegerField()
    name = models.CharField(max_length=40)
    age = models.IntegerField()
    