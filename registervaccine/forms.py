from kalender.models import Event
from .models import DataPendaftar
from django import forms

class FormPendaftaran(forms.ModelForm):
    username = forms.CharField(disabled=True, widget=forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    ))
    tanggal_vaksin = forms.DateField(disabled=True, widget=forms.DateInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'yyyy-mm-dd'
        }
    ))
    waktu_vaksin = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))
    NIK = forms.IntegerField(widget=forms.NumberInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Input your NIK (e.g. 3201123456780009)'
        }
    ))
    name = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Input your full name'
        }
    ))
    age = forms.IntegerField(widget=forms.NumberInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Input your age'
        }
    ))
    event = forms.CharField(disabled=True, widget=forms.TextInput(
        attrs={
            'class': 'form-control'
        }
    ))
    class Meta:
        model = DataPendaftar
        fields = ['username', 'tanggal_vaksin', 'waktu_vaksin', 'NIK', 'name','age']