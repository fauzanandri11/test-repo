from django.shortcuts import render
from .form import OriginalUserForm
from django.http import HttpResponseRedirect
from registervaccine.models import DataPendaftar
from django.contrib.auth.models import Group, User
import datetime
import calendar

def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year,month)[1])
    return datetime.date(year, month, day)

# Create your views here.
def user_profile(request):
    try:
        profile = request.user.originaluser
        regist = DataPendaftar.objects.get(username=request.user)
        admin = User.objects.get(username=request.user)
        second_date = add_months(regist.tanggal_vaksin, 1)
        context = {'profile':profile, 'regist': regist, 'second_date':second_date}
        return render(request,'profile_index.html', context)
    except:
        profile = request.user.originaluser
        context = {'profile':profile}
        return render(request,'profile_index.html', context)


def edit_profile(request):
    originaluser = request.user.originaluser
    form = OriginalUserForm(instance=originaluser)
    if request.method == 'POST':
        form = OriginalUserForm(request.POST, request.FILES,  instance=originaluser)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/profile')


    context = {'form':form}
    return render(request, 'edit_profile.html', context)
