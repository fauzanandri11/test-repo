from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from homepage.views import index
from userpage.views import user_regist, loginpage, logoutuser
from .views import edit_profile, user_profile

urlpatterns = [
    path('', user_profile, name='userprofile'), 
    path('edit-profile/', edit_profile, name='edit'),
     
]