from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from .forms import PostForm
from .models import Post
# Create your views here.
def article_index(request,post):
    li = get_object_or_404(Post,URLartikel=post)
    return render(request, 'index.html',{'li':li})

@login_required(login_url='/admin/login/')
def add_article(request):
    context={}
    form = PostForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()

    context['form'] = form
    return render(request, 'add.html', context)

def awal_article(request):
    li = Post.objects.all()
    return render(request,'awal.html',{'li':li})