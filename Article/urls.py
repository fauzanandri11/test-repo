from django.urls import path
from django.urls.resolvers import URLPattern

from .views import article_index,add_article,awal_article
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('',awal_article, name='awal'),
    path('/add-article',add_article,name='add article'),
    path('/<str:post>',article_index,name='article index'),
] + static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)