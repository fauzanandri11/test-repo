from django.db import models

# Create your models here.
class Feedback(models.Model):
    Name = models.CharField(max_length=128)
    Email = models.EmailField()
    Text = models.TextField()
    def __str__(self):
        Name = self.Text
        return Name