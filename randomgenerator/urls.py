from django.urls import path
from .views import index, add_note, json

urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_note, name='add_note'),
    path('json', json, name='json'),
]