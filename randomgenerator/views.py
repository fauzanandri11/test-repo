import random
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from django.http import JsonResponse
from .forms import FormNote
from .models import RandomNote

def index(request):
    maks= RandomNote.objects.latest('pk').pk
    getRandom = random.randrange(1, maks+1)
    temp = RandomNote.objects.get(pk=getRandom)
    return render(request, 'randomgenerator.html', {'temp' : temp})

@login_required(login_url='login')
def add_note(request):
    context = {}
    form = FormNote(request.POST or None)
    form.fields['writers'].initial = request.user

    if (form.is_valid and request.method == 'POST') :
        form.save()
    
    context['form'] = form
    return render(request, 'randomadd.html', context)



@login_required(login_url='login')
def json(request) :
    data = serializers.serialize('json', RandomNote.objects.filter(writers=request.user))
    return HttpResponse(data, content_type='application/json')

    

