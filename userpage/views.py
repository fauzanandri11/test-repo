import django
from django.http.response import JsonResponse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.hashers import make_password
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group, User
from django.views import View
from userpage.models import OriginalUser
from .form import UserRegist
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .decorators import unauthenticated_user, admin_only
import json
from django.contrib.auth.models import User


class PasswordValidation(View):
    def post(self, request):
        data = json.loads(request.body)
        password = data['password']
        if(len(password) < 8):
            return JsonResponse({'password_error':'password length at least 8 digits'}, status=400)
        return JsonResponse({'password_valid':True})

class UserValidation(View):
    def post(self, request):
        data = json.loads(request.body)
        username = data['username']
        
        if not str(username).isalnum():
            return JsonResponse({'username_error':'username must alphanumerics only'}, status=400)
        if User.objects.filter(username=username).exists():
            return JsonResponse({'username_error':'username already exists'}, status=400)
        
        return JsonResponse({'username_valid':True})

class UserEmailValidation(View):
    def post(self, request):
        data = json.loads(request.body)
        email = data['email']

        if User.objects.filter(email=email).exists():
            return JsonResponse({'email_error':'email already exists'}, status=400)
        
        return JsonResponse({'email_valid':True})



def user_regist(request):
    form = UserRegist()

    if request.method == 'POST':
        form  = UserRegist(request.POST)

        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            group = Group.objects.get(name='originaluser')
            user.groups.add(group)
            OriginalUser.objects.create(
                user=user
            )
            
            return HttpResponseRedirect('/login')

    context = {'form':form}
    return render(request, 'register_index.html', context)


def user_regist_admin(request):
    form = UserRegist()

    if request.method == 'POST':
        form  = UserRegist(request.POST)

        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            users = User.objects.get(username = username)
            users.is_staff = True
            users.is_superuser = True
            users.save()
            group = Group.objects.get(name='originaluser')
            user.groups.add(group)
            group = Group.objects.get(name='admin')
            user.groups.add(group)
            OriginalUser.objects.create(
                user=user
            )
            
            return HttpResponseRedirect('/login')

    context = {'form':form}
    return render(request, 'register_index_admin.html', context)



@unauthenticated_user
def loginpage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            messages.info(request, 'Username or Password is Incorrect')
            
    
    context = {}
    return render(request, 'login_index.html', context)


def logoutuser(request):
    logout(request)
    return HttpResponseRedirect('/')






