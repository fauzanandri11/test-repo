const usernameField = document.querySelector('#username');
const feedBackField = document.querySelector(".invalid-username")
usernameField.addEventListener('keyup', (e) => {
    console.log('777777', 777777);

    const userType=e.target.value;

    usernameField.classList.remove('is-invalid');
    feedBackField.style.display = 'none';

    if(userType.length > 0){
        fetch('/register/validation-username/', {
            body: JSON.stringify({username:userType}), 
            method:'POST',
        })
        .then(res => res.json())
        .then(data => {
            console.log('data: ', data);
            if(data.username_error){
                usernameField.classList.add('is-invalid');
                feedBackField.style.display = 'block';
                feedBackField.style.color = 'red';
                feedBackField.innerHTML = `<p>${data.username_error}<p>`
            }
        });
    }

});