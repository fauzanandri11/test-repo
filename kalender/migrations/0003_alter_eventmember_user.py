# Generated by Django 3.2.7 on 2021-11-05 08:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('kalender', '0002_alter_event_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventmember',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='event_members', to=settings.AUTH_USER_MODEL),
        ),
    ]
